/*
    KWin - the KDE window manager
    This file is part of the KDE project.

    SPDX-FileCopyrightText: 2007 Rivo Laks <rivolaks@hot.ee>

    SPDX-License-Identifier: GPL-2.0-or-later
*/

#include "invert_config.h"

#include <config-kwin.h>

// KConfigSkeleton
#include "invertconfig.h"
#include <kwineffects_interface.h>

#include <QAction>

#include <KActionCollection>
#include <KGlobalAccel>
#include <KLocalizedString>
#include <KPluginFactory>

#include <QVBoxLayout>

K_PLUGIN_CLASS(KWin::InvertEffectConfig)

namespace KWin
{

InvertEffectConfigForm::InvertEffectConfigForm(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
}

InvertEffectConfig::InvertEffectConfig(QWidget *parent, const QVariantList &args)
    : KCModule(parent, args)
{
    InvertConfig::instance(KWIN_CONFIG);
    m_ui = new InvertEffectConfigForm(this);

    QVBoxLayout *layout = new QVBoxLayout(this);
    layout->addWidget(m_ui);

    addConfig(InvertConfig::self(), m_ui);

    connect(m_ui->editor, &KShortcutsEditor::keyChange, this, &InvertEffectConfig::markAsChanged);

    // Shortcut config. The shortcut belongs to the component "kwin"!
    KActionCollection *actionCollection = new KActionCollection(this, QStringLiteral("kwin"));
    actionCollection->setComponentDisplayName(i18n("KWin"));
    actionCollection->setConfigGroup(QStringLiteral("Invert"));
    actionCollection->setConfigGlobal(true);

    QAction *a = actionCollection->addAction(QStringLiteral("Invert"));
    a->setText(i18n("Toggle Invert Effect"));
    a->setProperty("isConfigurationAction", true);
    KGlobalAccel::self()->setDefaultShortcut(a, QList<QKeySequence>() << (Qt::CTRL | Qt::META | Qt::Key_I));
    KGlobalAccel::self()->setShortcut(a, QList<QKeySequence>() << (Qt::CTRL | Qt::META | Qt::Key_I));

    QAction *b = actionCollection->addAction(QStringLiteral("InvertWindow"));
    b->setText(i18n("Toggle Invert Effect on Window"));
    b->setProperty("isConfigurationAction", true);
    KGlobalAccel::self()->setDefaultShortcut(b, QList<QKeySequence>() << (Qt::CTRL | Qt::META | Qt::Key_U));
    KGlobalAccel::self()->setShortcut(b, QList<QKeySequence>() << (Qt::CTRL | Qt::META | Qt::Key_U));

    m_ui->editor->addCollection(actionCollection);
}

InvertEffectConfig::~InvertEffectConfig()
{
    // Undo (only) unsaved changes to global key shortcuts
    m_ui->editor->undo();
}

void InvertEffectConfig::save()
{
    m_ui->editor->save(); // undo() will restore to this state from now on
    KCModule::save();
    OrgKdeKwinEffectsInterface interface(QStringLiteral("org.kde.KWin"),
                                         QStringLiteral("/Effects"),
                                         QDBusConnection::sessionBus());
    interface.reconfigureEffect(QStringLiteral("invert"));
}

} // namespace

#include "invert_config.moc"
